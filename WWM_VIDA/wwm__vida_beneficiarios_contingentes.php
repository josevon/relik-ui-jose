<div class="col-sm-12">
    <div class=" container container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">Beneficiarios Principales
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table table-hover" id="BenefitTable" ui-jq="dataTable" ui-options="options">
                            <thead>
                            <tr>
                                <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                                            Comman Practice Followed
                                            -->

                                <th style="width:30%">Apellidos,Nombres</th>
                                <th style="width:30%">Documento de Identidad</th>

                                <th style="width:10%">Parentesco</th>

                                <th style="width:30%">%distribucion</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryName_1"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiarID_1"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-3">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryParentType_1"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input id="benfitDistribution_1" type="text" data-a-sign="$ "
                                                   class="autonumeric form-control"
                                                   ui-jq="autoNumeric" ui-options="'init'">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryName_2"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiarID_2"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-3">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryParentType_2"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input id="benfitDistribution_2" type="text" data-a-sign="$ "
                                                   class="autonumeric form-control"
                                                   ui-jq="autoNumeric" ui-options="'init'">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryName_3"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle ">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiarID_3"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-3">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input type="text" id="beneficiaryParentType_3"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </td>
                                <td class="v-align-middle">
                                    <div class="col-sm-12">
                                        <div pg-form-group
                                             class="form-group ">
                                            <input id="benfitDistribution_3" type="text" data-a-sign="$ "
                                                   class="autonumeric form-control"
                                                   ui-jq="autoNumeric" ui-options="'init'">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h5>Si a mi fallecimiento alguno de mis Beneficiarios Contingentes es menor de edad, el
                            beneficio
                            correspondiente será entregado a</h5>
                        <div class='row'>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">w
                                    <label>Nombre/Apellido</label>
                                    <input type="text" name="formWorkspaceCompany"
                                           placeholder=" Nombre de empresa en la que labora"
                                           class="form-control">


                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Parentesco</label>
                                    <input type="text" name="formWorkspaceCompany"
                                           placeholder=" Nombre de empresa en la que labora"
                                           class="form-control">


                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Cedula/Pasaporte</label>
                                    <input type="text" name="formWorkspaceCompany"
                                           placeholder=" Nombre de empresa en la que labora"
                                           class="form-control">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
