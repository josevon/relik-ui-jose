<div class="col-lg-12">
    <div class=" container    container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">Informacion del Contratante
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group="" class="form-group form-group-default required">
                                <label class="text-null no-margin fade">Tipo de plan</label>
                                <select id="planType" class="form-control">
                                    <option value="optimunPlus">WWTerm</option>
                                    <option value="optimun">
                                        <W></W>
                                        WSurvivor
                                    </option>
                                    <option value="security">WWTerm Value</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group="" class="form-group form-group-default required">
                                <label class="text-null no-margin fade">Opciones Deducibles</label>
                                <select id="planOption" name="formPersonalIdType" class="form-control">
                                    <option value="deduc_option_1">Opcion 1</option>
                                    <option value="deduc_option_2">Opcion 2</option>
                                    <option value="deduc_option_3">Opcion 3</option>
                                    <option value="deduc_option_4">Opcion 4</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <h5>Vida Basica</h5>
                    <div class='row'>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Suma asegurada</label>
                                <input type="text" name="formWorkspaceCompany"
                                       placeholder=" Nombre de empresa en la que labora"
                                       class="form-control">


                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Prima</label>
                                <input type="text" name="formWorkspaceCompany"
                                       placeholder=" Nombre de empresa en la que labora"
                                       class="form-control">


                            </div>
                        </div>
                    </div>
                    <h5>Supervivencia</h5>
                    <div class='row'>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Suma asegurada</label>
                                <input type="text" name="formWorkspaceCompany"
                                       placeholder=" Nombre de empresa en la que labora"
                                       class="form-control">


                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Prima</label>
                                <input type="text" name="formWorkspaceCompany"
                                       placeholder=" Nombre de empresa en la que labora"
                                       class="form-control">


                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <h5>Prima total: </h5>
                        </div>
                        <div class="col-sm-6">
                            <h5>$151 </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
