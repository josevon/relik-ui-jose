<div class="col-lg-6">
    <div class=" container    container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">Informacion del Contratante
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Nombre</label>
                                <input type="text" name="formPersonalName"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Segundo Nombre</label>
                                <input type="text" name="formPersonalMiddleName"
                                       class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Apellido</label>
                                <input type="text" name="formPersonalLastName"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Segundo Apellido</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <select name="formPersonalIdType" class="form-control">
                                <option value="CED">Cedula</option>
                                <option value="PAS">Pasaporte</option>
                            </select>
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <input type="text" name="formPersonalId"
                                       data-required="1"
                                       class="form-control" placeholder="" ="
                                                                                        form-group form-group-default"
                                "">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Sexo</label>
                                <select name="formPersonalGender" class="form-control">
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Fecha de Nacimiento</label>
                                <input type="text" data-required="1"
                                       class="form-control"
                                       name="formPersonalBirthday"
                                       placeholder="Dia/Mes/Año"
                                       required="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Estado Civil</label>
                                <select name="formPersonalCivil" class="form-control">
                                    <option value="S">Soltero</option>
                                    <option value="C">Casado</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Nacionalidad</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Edad</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>peso (LBS)</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Altura (Pies/Plgds)</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Tel. Residencia</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Tel. Oficina</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Tel. Celular</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Direccion</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>E-mail</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Pais</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Ciudad</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Ingreso Anual</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Moneda</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Perfil termina-->