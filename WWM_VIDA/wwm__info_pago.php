<div class="col-lg-6">
    <div class=" container    container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">Informacion del Contratante
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group="" class="form-group form-group-default required">
                                <label class="text-null no-margin fade">Metodo de Pago</label>
                                <select name="formPersonalIdType" class="form-control">
                                    <option value="payment_method_check">Check</option>
                                    <option value="payment_method_tcr">TCR</option>
                                    <option value="payment_method_cash">Efectivo</option>
                                    <option value="payment_method_voluntary">Voluntario</option>
                                    <option value="payment_method_transfer">Transferencia</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group="" class="form-group form-group-default required">
                                <label class="text-null no-margin fade">Frecuencia de Pago</label>
                                <select name="formPersonalIdType" class="form-control">
                                    <option value="payment_frecuency_trimestra">Trimestral</option>
                                    <option value="payment_frecuency_semestral">Semestral</option>
                                    <option value="payment_frecuency_anual">Anual</option>


                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>