<div class="col-lg-12">
    <div class=" container container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">Informacion Laboral
                </div>
                <div class="card-block">

                    <div class="row">
                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Nombre de la Compañia</label>
                                <input type="text" name="formWorkspaceProfession"
                                       placeholder="Nombre de Profesion"
                                       class="form-control">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Ocupacion</label>
                                <input type="text" name="formWorkspaceProfession"
                                       placeholder="Nombre de Profesion"
                                       class="form-control">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Razon o funcion social en la compañia</label>
                                <input type="text" name="formWorkspaceProfession"
                                       placeholder="Nombre de Profesion"
                                       class="form-control">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Años de empleo en la compañia</label>
                                <input type="text" name="formWorkspaceCompany"
                                       placeholder=" Nombre de empresa en la que labora"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Deberes (Favor dar Detalles)</label>
                                <textarea type="text" name="formWorkspaceAddress"
                                          placeholder="Descripcion corta de su trabajo"
                                          style="max-width: 500px; min-width: 450px; min-height: 40px; max-height: 90px">
                                 </textarea>

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Dirección</label>
                                <input type="text" name="formWorkspaceAddress"
                                       placeholder="Dirección de Trabajo"
                                       class="form-control">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>