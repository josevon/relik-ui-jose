<div class="col-lg-6">
    <div class=" container    container-fixed-lg">
        <!-- START card -->
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title"> Informacion del Empleador
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Nombre</label>
                                <input type="text" name="formPersonalName"
                                       class="form-control" required>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Apellido</label>
                                <input type="text" name="formPersonalLastName"
                                       class="form-control" required>
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <select name="formPersonalIdType" class="form-control">
                                <option value="CED">Cedula</option>
                                <option value="PAS">Pasaporte</option>
                            </select>
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <input type="text" name="formPersonalId"
                                       data-required="1"
                                       class="form-control" placeholder="" ="
                                                                                        form-group form-group-default"
                                "">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Sexo</label>
                                <select name="formPersonalGender" class="form-control">
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Nacionalidad</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Telefono Residencia</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Telefono Celular</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Email</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Ingreso Anual</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Direccion de Residencia</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Ciudad</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Pais</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Apdo.Postal</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Pais de Residencia</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Parentesco con el Asegurado</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Es usted un PEP?</label>
                                <select name="formPersonalGender" class="form-control">
                                    <option value="M">Si</option>
                                    <option value="F">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div pg-form-group
                                 class="form-group form-group-default required">
                                <label>Rep. Legal(Persona Juridica)</label>
                                <input type="text" name="formPersonalSsecondLastName" class="form-control">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Perfil termina-->