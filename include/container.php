<div class="page-container ">
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
            <div class="bg-white">
                <div class="container">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Form Elements</li>
                    </ol>
                </div>
            </div>
            <!-- START JUMBOTRON -->
            <div class="jumbotron">
                <div class=" container p-l-0 p-r-0   container-fixed-lg sm-p-l-0 sm-p-r-0">
                    <div class="inner">
                        <!-- START BREADCRUMB -->
                        <div class="row">
                            <div class="col-xl-7 col-lg-6 ">
                                <!-- START card -->
                                <div class="card card-default">
                                    <div class="card-block text-center">
                                        <img class="image-responsive-height demo-mw-600"
                                             src="assets/img/demo/form_hero.gif" alt="">
                                    </div>
                                </div>
                                <!-- END card -->
                            </div>
                            <div class="col-xl-5 col-lg-6 ">
                                <!-- START card -->
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Getting started
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <h3>One of the most underestimated elements of design is typography. However,
                                            it’s
                                            critical in both print and web media. We Made it Perfect in both.</h3>
                                        <br>
                                        <div>
                                            <div class="profile-img-wrapper m-t-5 inline">
                                                <img width="35" height="35" src="assets/img/profiles/avatar_small.jpg"
                                                     alt="" data-src="assets/img/profiles/avatar_small.jpg"
                                                     data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                                                <div class="chat-status available">
                                                </div>
                                            </div>
                                            <div class="inline m-l-10">
                                                <p class="small hint-text m-t-5">VIA senior product manage
                                                    <br> for UI/UX at REVOX</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END JUMBOTRON -->
            <!-- START CONTAINER FLUID -->
            <div class=" container container-fixed-lg">
                <div class="row">

