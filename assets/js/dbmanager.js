function objectManager() {
    this.version = "1.0";
    this.available = false;
    this.mydb;
    this.request;
    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

};


objectManager.prototype.init = function () {
    var open = this.indexedDB.open("objectManager", 1);

    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore("objectStore", {keyPath: "id"});
        var index = store.createIndex("NameIndex", ["key", "value"]);
    };

    open.onsuccess = function () {
        // Start a new transaction
        var db = open.result;
        var tx = db.transaction("objectStore", "readwrite");
        var store = tx.objectStore("objectStore");
        var index = store.index("NameIndex");

        // Add some data
        //store.put({id: "autosave", key: "soto"});
        //store.put({key: "paco", value: "coco,sala" });

        // // Query the data
        // var getJohn = store.get(12345);
        // var getBob = index.get(["Smith", "Bob"]);
        //
        // getJohn.onsuccess = function() {
        //     console.log(getJohn.result.name.first);  // => "John"
        // };
        //
        // getBob.onsuccess = function() {
        //     console.log(getBob.result.name.first);   // => "Bob"
        // };

        // Close the db when the transaction is done
        tx.oncomplete = function () {
            db.close();
        };
    }
};


objectManager.prototype.put = function (key, value) {
    var open = this.indexedDB.open("objectManager", 1);

    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore("objectStore", {keyPath: "id"});
        var index = store.createIndex("NameIndex", ["key", "value"]);
    };

    open.onsuccess = function () {
        var db = open.result;
        var tx = db.transaction("objectStore", "readwrite");
        var store = tx.objectStore("objectStore");
        var index = store.index("NameIndex");

        store.put({id: key, object: value});

        tx.oncomplete = function () {
            db.close();
        };
    }
};

objectManager.prototype.get = function (key, callback) {
    var open = this.indexedDB.open("objectManager", 1);

    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore("objectStore", {keyPath: "id"});
        var index = store.createIndex("NameIndex", ["key", "value"]);
    };

    open.onsuccess = function () {
        var db = open.result;
        var tx = db.transaction("objectStore", "readwrite");
        var store = tx.objectStore("objectStore");
        var index = store.index("NameIndex");

        var object = store.get(key);

        object.onsuccess = function (e) {
            if (typeof callback !== 'undefined') {
                if (typeof e.target.result !== 'undefined') {
                    callback(e.target.result.object);
                } else {
                    callback(null);
                }
            }
        };

        object.onerror = function (e) {
            if (typeof callback !== 'undefined') {
                callback(null);
            }
        };


        tx.oncomplete = function () {
            db.close();
        };
    }

};

objectManager.prototype.delete = function (key) {
    var open = this.indexedDB.open("objectManager", 1);

    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore("objectStore", {keyPath: "id"});
        var index = store.createIndex("NameIndex", ["key", "value"]);
    };

    open.onsuccess = function () {
        var db = open.result;
        var tx = db.transaction("objectStore", "readwrite");
        var store = tx.objectStore("objectStore");
        var index = store.index("NameIndex");

        var object = store.delete(key);

        object.onsuccess = function (e) {
            console.log("DELETED!");
            if (typeof callback !== 'undefined') {
                callback(e.target.result.object);
            }
        };

        object.onerror = function (e) {
            if (typeof callback !== 'undefined') {
                callback(null);
            }
        };

        tx.oncomplete = function () {
            db.close();
        };
    }

}

objectManager.prototype.log = function (message) {
    console.log("[dbManager]: " + message);
};

var _objectManager = new objectManager();
_objectManager.init();



//var _dbManager = new dbManager();
//_dbManager.init();


// This works on all devices/browsers, and uses IndexedDBShim as a final fallback


// Open (or create) the database
