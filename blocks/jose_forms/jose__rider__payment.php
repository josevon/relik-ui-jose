<div class=" container    container-fixed-lg">
    <!-- START card -->
    <div class="col-lg-12">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title">
                </div>
                <h5 class="page-title">Formas de pago y deducibles</h5>
            </div>
            <h6></h6>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group="" class="form-group form-group-default required">
                            <label class="text-null no-margin fade">Tipo de plan</label>
                            <select id="planType" class="form-control">
                                <option value="optimunPlus">Optmiun Plus</option>
                                <option value="optimun">Optmiun</option>
                                <option value="security">Security</option>
                                <option value="essence">Essence</option>
                                <option value="exclusive">Exclusive Int</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group="" class="form-group form-group-default required">
                            <label class="text-null no-margin fade">Opciones Deducibles</label>
                            <select id="planOption" name="formPersonalIdType" class="form-control">
                                <option value="deduc_option_1">Opcion 1</option>
                                <option value="deduc_option_2">Opcion 2</option>
                                <option value="deduc_option_3">Opcion 3</option>
                                <option value="deduc_option_4">Opcion 4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Prima </label>
                            <input type="text" id="suraID"
                                   class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Tarifa</label>
                            <input type="text" id="riderLicence"
                                   class="form-control" required>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <!-- START PANEL -->
                        <h5 class="page-title">Informacion del corredor</h5>

                        <div class="row">
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Codigo Sura </label>
                                    <input type="text" id="suraID"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Licensia </label>
                                    <input type="text" id="riderLicence"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Nombre o Razon social </label>
                                    <input type="text" id="riderName"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Sucursal Sura </label>
                                    <input type="text" id="suraOffice"
                                           class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
