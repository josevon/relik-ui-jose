<div class=" container    container-fixed-lg">
    <!-- START card -->
    <div class="col-lg-6">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title"><h6>Formas de Pago y Deducibles</h6>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12">
                        <!-- START PANEL -->
                        <h3 style="text-align: center">
                        </h3>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Codigo Sura </label>
                                    <input type="text" id="suraID"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Licensia </label>
                                    <input type="text" id="riderLicence"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Nombre o Razon social </label>
                                    <input type="text" id="riderName"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div pg-form-group
                                     class="form-group form-group-default required">
                                    <label>Sucursal Sura </label>
                                    <input type="text" id="suraOffice"
                                           class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>