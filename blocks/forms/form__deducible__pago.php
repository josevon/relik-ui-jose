<div class=" container    container-fixed-lg">
    <!-- START card -->
    <div class="col-lg-6">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title"><h6>Formas de Pago y Deducibles</h6>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group="" class="form-group form-group-default required">
                            <label class="text-null no-margin fade">Tipo de plan</label>
                            <select id="planType" class="form-control">
                                <option value="optimunPlus">Optmiun Plus</option>
                                <option value="optimun">Optmiun</option>
                                <option value="security">Security</option>
                                <option value="essence">Essence</option>
                                <option value="exclusive">Exclusive Int</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group="" class="form-group form-group-default required">
                            <label class="text-null no-margin fade">Opciones Deducibles</label>
                            <select id="planOption" name="formPersonalIdType" class="form-control">
                                <option value="deduc_option_1">Opcion 1</option>
                                <option value="deduc_option_2">Opcion 2</option>
                                <option value="deduc_option_3">Opcion 3</option>
                                <option value="deduc_option_4">Opcion 4</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>