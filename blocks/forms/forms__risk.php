<div class="col-lg-12">
    <!-- START card -->
    <div class="card card-default">
        <div class="card-header ">
            <div class="card-title">
                Option #one
            </div>
        </div>
        <div class="card-block">
            <h5>
                Salud y Bienestar
            </h5>
            <br>
            <h6 class="semi-bold">A).Enfermedad Comun/General/Profesional</h6>
            <h7>Gastos medicos por urgencia, consulta medica general o especializada, maternidad</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="card-block">
            <br>
            <h6 class="semi-bold">B). Enfermedad Grave desde la prestacion de salud</h6>
            <h7>Gastos de hospitalizacion y cirugia tratamiento , rehabilitaciones</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="card-block">
            <br>
            <h6 class="semi-bold">C). Enfermedad grave desde la indemnizacion</h6>
            <h7>Indemnizacion en vida, conservacion de ingresos, posible dependendencias de otros</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <h5>Proteger tus Ingresos</h5>
            <br>
            <h6 class="semi-bold">D). Invalidez por Enfermedad</h6>
            <h7>Indemnizacion en vida, conservacion de ingresos, posible dependendencias de otros</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <br>
            <h6 class="semi-bold">E). Invalidez por Accidente</h6>
            <h7>Perdida de capacidad de generacion de ingresos, posible dependencias a otros</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <br>
            <h6 class="semi-bold">F). Fallecimiento</h6>
            <h7>Perdida parcial o total de los ingresos familiares</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <br>
            <h6 class="semi-bold">G). Incapacidad laboral temporal por accidente o enfermedad</h6>
            <h7>Interrupcion temporal de ingresos</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-block">
            <h5>
                Qué tan expuesto estas a los riesgos relacionados con tu:
            </h5>
            <br>
            <h6 class="semi-bold">A).</h6>
            <h7>Gastos medicos por urgencia, consulta medica general o especializada, maternidad</h7>
            <br>
            <br>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Niveles de Expocision</label>
                            <select id="expositionLevel_1" class="form-control">
                                <option value="N.A">N.A</option>
                                <option value="Baja">Baja</option>
                                <option value="Media">Media</option>
                                <option value="Alta">Alta</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Imapacto Economico</label>
                            <select id="economicImpact_1" class="form-control">
                                <option value="Bajo">Bajo</option>
                                <option value="Medio">Medio</option>
                                <option value="Alto">Alto</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div pg-form-group
                             class="form-group form-group-default required">
                            <label>Aporte a caja del seguro?</label>
                            <select id="aporteCajadelSeguro" class="form-control">
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Con quien?</label>
                            <input id="insuranceCarrier" type="text" placeholder="Default input"
                                   class="form-control">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
