<div class=" container    container-fixed-lg">
    <!-- START card -->
    <div class="col-lg-12">
        <div class="card card-default">
            <div class="card-header ">
                <div class="card-title"><h6>Beneficiarios Contingentes</h6>
                </div>
                <div class="pull-right">
                    <div class="col-xs-12">
                        <button id="Show-1" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add
                            row
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-block">
                <table class="table table-hover demo-table-dynamic table-responsive-block"
                       id="contingenteBeneficiaryWWM">
                    <thead>
                    <tr>
                        <th>Nombre Y apellido</th>
                        <th>Identificacion</th>
                        <th>Nacionalidad</th>
                        <th>Parentesco</th>
                        <th>%</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <h7>Si a mi fallecimiento alguno de mis beneficiarios es menor de edad el beneficio correspondiente será
                    entregado a
                </h7>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-default required ">
                            <label>Nombre</label>
                            <input type="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group form-group-default required ">
                            <label>Parentesco</label>
                            <input type="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group form-group-default required ">
                            <label>Cedula Pasaporte</label>
                            <input type="email" class="form-control" required>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade stick-up" id="beneficiaryModal" tabindex="-1" role="dialog"
             aria-labelledby="addNewAppModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="pg-close fs-14"></i>
                        </button>
                        <h4 class="p-b-5"><span class="semi-bold">New</span> App</h4>
                    </div>
                    <div class="modal-body">
                        <p class="small-text">Create a new app using this form, make sure you fill them all</p>
                        <form role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Nombre y Apellido</label>
                                        <input id="beneficiaryName" type="text" class="form-control"
                                               placeholder="Name of your app">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Identificacion</label>
                                        <input id="beneficiaryId" type="text" class="form-control"
                                               placeholder="Tell us more about it">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Nacionalidad</label>
                                        <input id="beneficiaryNationality" type="text" class="form-control"
                                               placeholder="your price">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Parentesco</label>
                                        <input id="beneficiaryRelationship" type="text" class="form-control"
                                               placeholder="a note">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Porcentaje</label>
                                        <input id="beneficiaryPercentage" type="text" class="form-control"
                                               placeholder="a note">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="add-Beneficiary" type="button" class="btn btn-primary  btn-cons">Add</button>
                        <button type="button" class="btn btn-cons" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- END card -->
    </div>
</div>
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/tether/js/tether.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js"
        type="text/javascript"></script>
<script src="assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js"
        type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="assets/plugins/datatables-responsive/js/lodash.min.js"></script>

<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/datatables.js" type="text/javascript"></script>
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
